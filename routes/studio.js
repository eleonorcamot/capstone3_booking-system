const router = require('express').Router();
const Studio = require('./../models/Studio');
const multer = require ('multer');
const passport = require ('passport')


// multer settings
const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'assets/images')
	},
	filename: function (req, file, cb) {
		// console.log(file);
		cb(null, Date.now() + "-" + file.originalname)
	}
})

const upload = multer({ storage: storage });

const adminOnly = (req,res,next) => {
	
	if(req.user.isAdmin){
		next()
	} else {
		res.status(403).send({
			error: "Forbidden"
		})
	}

}

//router level middleware
router.use((req,res,next)=>{
	// console.log(req);
	// console.log(req.ip + " " + req.originalUrl + " " + new Date(Date.now()));
	next()
})

// products endpoints
router.get('/', (req,res, next) => {
	Studio.find()
	.then(studios => {
		res.send(studios)
	})
	.catch(next)
})

router.get('/:id', (req, res, next) =>{

	Studio.findById(req.params.id)
	.then(studio => res.send(studio))
	.catch(next)
}) 

router.post('/', 
	passport.authenticate('jwt', {session:false}),
	adminOnly,
	upload.single('image'), 
	(req, res, next) =>{
	
	req.body.image = "public/" + req.file.filename

	Studio.create(req.body)
	.then(studio => {
		res.send(studio)
	})
	.catch(next)

})

router.put('/:id', 
	passport.authenticate('jwt', {session:false}), 
	adminOnly,
	upload.single('image'), 
	(req, res, next) =>{
	
	if (req.file){
		req.body.image = "public/" + req.file.filename;
	}

	Studio.findByIdAndUpdate(
		req.params.id,
		req.body,
		{new: true}
	)
	.then(studio => res.send(studio))
	.catch(next)
})

router.delete('/:id', 
	passport.authenticate('jwt', {session:false}),
	adminOnly,
	(req, res) =>{

	Studio.findByIdAndDelete(req.params.id)
	.then(studio =>  res.send(studio))

})


module.exports = router;