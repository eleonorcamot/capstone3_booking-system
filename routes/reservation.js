const router = require('express').Router();
const Reservation = require ('./../models/Reservation');
const passport = require('passport');
const Studio = require('./../models/Studio');


const authenticate = passport.authenticate('jwt', {session:false});

router.get('/',authenticate,  (req ,res, next) => {
	let filter = {}
	if (!req.user.isAdmin){
		filter = {customerId: req.user._id}
	}

	Reservation.find(filter)
	.populate('customerId', ['email', 'fullname'] )
	.populate('bookings.studioId', ['name', 'image', 'description', 'capacity'])
	
	.then(reservations => res.send(reservations))
	.catch(next)
})

router.get('/:id', authenticate, (req,res,next) =>{
	//use findOne than findById if you want to 
	let filter = {_id: req.params.id}

	if (!req.user.isAdmin){
			filter ={
			...filter,
			customerId: req.user._id
		}
	}

	Reservation.findOne(filter)
	.populate('customerId', ['email', 'fullname'] )
	.populate('bookings.productId', ['name', 'image', 'description'])
	.then(reservation => {
			if(reservation){
				res.send(reservation)
			}else {
				res.status(403).send("Unauthorized")
			}
	})
	.catch(next)
})


router.post('/', authenticate, (req,res,next) =>{
	let customerId = req.user._id
	let startDate = req.body.startDate

	const arrayOfIds = req.body.bookings.map(booking =>{
		return booking.studioId
	})

	Studio.find({_id: {$in: arrayOfIds}})
	.then(studios => {
		let total = 0
		let bookingListSummary = studios.map(studio => {
			let matchedStudio = {};
			req.body.bookings.forEach(booking => {
				if(booking.studioId == studio._id){
					matchedStudio= {
						studioId: studio._id,
						price: studio.price,
						name: studio.name,
						hour: booking.hour,
						subtotal: booking.hour * studio.price
					}
					total += matchedStudio.subtotal;
				}
			})
			return matchedStudio;
		})
		
		Reservation.create({
			customerId,
			bookings: bookingListSummary,
			total,
			startDate
		})
		.then(reservation =>{
			res.send(reservation)
		})
		.catch(next)
	})
})

router.put('/:id', authenticate, (req,res,next) => {
			req.user.isAdmin ? next() : res.status(403).send("Unauthorized")
},(req,res,next)=>{
	Reservation.findByIdAndUpdate(
		req.params.id,
		{isComplete : req.body.isComplete}, //only isComplete will be updated
		{new: true}
	)
	.then(reservation => res.send(reservation))
	.catch(next)
})


module.exports = router;