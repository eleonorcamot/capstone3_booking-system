const router = require('express').Router();
const Studio = require('./../models/Studio');

router.post('/', (req,res,next)=>{
	
	let startDate = req.body.startDate;

	const arrayOfIds = req.body.bookings.map(booking =>{
		return booking.studioId
	})

	Studio.find({_id: {$in: arrayOfIds}})
	.then(studios => {
		let total = 0
		let bookingListSummary = studios.map(studio => {
			let matchedStudio = {};
			req.body.bookings.forEach(booking => {
				if(booking.studioId == studio._id){
					matchedStudio= {
						studioId: studio._id,
						price: studio.price,
						name: studio.name,
						hour: booking.hour,
						subtotal: booking.hour * studio.price
					}
					total += matchedStudio.subtotal;
				}
			})
			return matchedStudio;
		}) 
		res.send({bookings: bookingListSummary, total, startDate})
	})
})

module.exports = router;
