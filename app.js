// console.log('test');

const express = require('express')
const app = express()
const mongoose = require('mongoose')
const cors = require('cors')
const port = process.env.PORT || 4000


// Routes required
const studios = require('./routes/studio');
const users = require('./routes/user');
const reservations = require ('./routes/reservation');
const bookings = require('./routes/booking');
require('dotenv').config();


// connection to database
mongoose.connect(process.env.ATLAS, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true,
	useFindAndModify: false
});


// ensure that connection is successful
mongoose.connection.on('connected', ()=>{
	console.log('Connected to database');
});


// middleware
app.use((req,res,next)=> {
	console.log(req.method);
	next();
});

app.use(express.json());
app.use(cors());


app.use('/studios', studios)
app.use('/users', users)
app.use('/reservations', reservations)
app.use('/bookings', bookings)

//serve a static file
app.use('/public', express.static('assets/images'))

app.get('/', (req,res) => {
	res.send('Hello World!')
})

// Error handling middleware
app.use((err, req,res, next) => {
	// console.log(err.message);
	res.status(400).send({
		error: err.message
	})
})


app.listen(port, ()=>{
	console.log(`App is listening at port ${port}`)
})


