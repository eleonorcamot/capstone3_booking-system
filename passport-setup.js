const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./models/User')

const opts = {};

opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
	

opts.secretOrKey = 'sample_key'; 

passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
	 	
	// anonymous callback function
    User.findOne({_id: jwt_payload._id}, function(err, user) {
        if (err) {
            return done(err, false);
        }
        if (user) {
            return done(null, user);
            //user data - will be the response if there's a user
        } else {
            return done(null, false);
            // or you could create a new account
        }
    });
}));



