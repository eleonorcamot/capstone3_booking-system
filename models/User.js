const mongoose = require ('mongoose')
const Schema = mongoose.Schema;

const UserSchema = new Schema ({
	fullname: {
		type: String,
		required: [true, 'Fullname is required']
	},
	email : {
		type: String,
		required: [true, 'Email is required'],
		unique: true
	},
	password: {
		type: String,
		required: [true, 'Password is required'],
		minlength: [8, 'Password should be at least 8 characters.']
	},
	isAdmin: {
		type: Boolean,
		default: 0
	}

}, 

	{timestamps: true}
);



module.exports = mongoose.model('User', UserSchema)