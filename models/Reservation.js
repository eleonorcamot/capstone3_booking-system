const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReservationSchema = new Schema({
	customerId: {
		type: mongoose.Schema.Types.ObjectId, //unstructured
		ref: 'User', 
		required: [true, "Customer ID is required"]
	},
	total: {
		type: Number,
		required: [true, "Total should not be empty"],
		min: 0.01
	},
	isComplete:{
		type: Boolean,
		default: false
	},
	startDate:{
		type: String
	},
	bookings: [
		{
			studioId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Studio'
			},
			hour: {
				type: Number,
				required: [true, "Hours needed required"]
			},
			price: {
				type: Number,
				required: [true, "Studio price required"]
			},
			subtotal: {
				type:Number,
				required: [true, "Subtotal required"]
			},
		}
	]

},{timestamps: true})

module.exports = mongoose.model('Reservation', ReservationSchema);
