const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const StudioSchema = new Schema ({
	name: {
		type:String,
		unique: true,
		required: [true,  'Name is required.']
	},
	price: {
		type:Number,
		required: [true, 'Price is required.'],
		min: 0.01
	},
	description: {
		type: String,
		required: [true, 'Description is required.']
	},
	capacity: {
		type: String,
		required: [true, 'Capacity is required.']
	},
	category: {
		type: String,
		required: [true, 'Category is required.']
	},
	image: {
		type: String,
		required: [ true, 'Image is required.']
	}

},{
	timestamps: true
})


module.exports = mongoose.model('Studio', StudioSchema);